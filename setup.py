from setuptools import setup, find_packages
import os

parent_folder = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(parent_folder, "requirements.txt")) as req:
    INSTALL_REQUIRES = req.read().split("\n")

setup(
    author="Julio Alvia",
    author_email="jealvia@hotmail.com",
    name="hexagonal-python-graphql",
    license="",
    description="Base hexagonal fastapi graphql",
    version="1.0.0",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.9.2",
    install_requires=INSTALL_REQUIRES,
)
