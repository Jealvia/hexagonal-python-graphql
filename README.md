# Arquitectura hexagonal

## Ejecutar

- docker compose up -d
- Abrir el puerto http://0.0.0.0:8000/graphql

## Mutacion ejemplo
```sh
mutation{
  registerProduct(product:{
    name: "prueba",
    price: 15
  })
}
```