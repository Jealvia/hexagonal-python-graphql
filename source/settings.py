import os
import pathlib
from functools import lru_cache
from dotenv import load_dotenv
from pydantic import BaseSettings


# Load .env vars
load_dotenv(pathlib.Path(".").parent / ".env")

MONGO_URL = os.getenv("MONGO_URL")
MONGO_DATABASE = os.getenv("MONGO_DATABASE")
POSTGRESQL_DATABASE = os.getenv("POSTGRESQL_DATABASE")
FIREBASE = os.getenv("FIREBASE")
RABBITMQ = os.getenv("RABBIT_DIR")
PHONENUMBER_ID = os.getenv("PHONENUMBER_ID")
ACCESS_TOKEN = os.getenv("ACCESS_TOKEN")
MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
MAIL_SENDER = os.getenv("MAIL_SENDER")
MAIL_KEY = os.getenv("MAIL_KEY")


@lru_cache
def get_env_filename():
    runtime_env = os.getenv("ENV")
    return f".env.{runtime_env}" if runtime_env else ".env"


class EnvironmentSettings(BaseSettings):
    API_VERSION: str
    APP_NAME: str
    DATABASE_DIALECT: str
    DATABASE_HOSTNAME: str
    DATABASE_NAME: str
    DATABASE_PASSWORD: str
    DATABASE_PORT: int
    DATABASE_USERNAME: str
    DEBUG_MODE: bool

    JWT_PUBLIC_KEY: str
    JWT_PRIVATE_KEY: str
    REFRESH_TOKEN_EXPIRES_IN: int
    ACCESS_TOKEN_EXPIRES_IN: int
    JWT_ALGORITHM: str
    SECRET_KEY: str

    class Config:
        env_file = get_env_filename()
        env_file_encoding = "utf-8"


@lru_cache
def get_environment_variables():
    return EnvironmentSettings()
