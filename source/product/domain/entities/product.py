from pydantic import BaseModel


class Product(BaseModel):
    """Entity for product"""

    id: int
    name: str
    price: float
