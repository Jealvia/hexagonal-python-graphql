from typing import Optional
import strawberry


@strawberry.input(description="Product create schema")
class GProductSchema:
    """Product create schema"""

    name: str
    price: float


@strawberry.input(description="Product update schema")
class GProductEditSchema:
    """Product edit schema"""

    id: int
    name: Optional[str] = None
    price: Optional[float] = None


@strawberry.input(description="Get one product schema")
class GProductIdInput:
    """Get one product schema"""

    id: str
