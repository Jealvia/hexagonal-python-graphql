from dataclasses import dataclass


@dataclass
class ProductSchema:
    """Product create schema"""

    name: str
    price: float
