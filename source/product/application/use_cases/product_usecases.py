from source.product.application.ports.primary.iproductprimaryport import IProductPrimaryPort
from source.product.application.ports.secondary.iproductsecondaryport import (
    IProductSecondaryPort,
)
from source.product.domain.dtos.product import ProductSchema
from source.product.domain.entities.product import Product


class ProductUseCases(IProductPrimaryPort):
    """Product use case"""

    secondary_port: IProductSecondaryPort

    def __init__(self, repository: IProductSecondaryPort) -> None:
        self.secondary_port = repository

    async def save_product(self, product: ProductSchema) -> int:
        temp_product = Product(id=0, name=product.name, price=product.price)
        result = await self.secondary_port.save_product(temp_product)
        return result
