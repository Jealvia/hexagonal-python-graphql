from abc import ABC, abstractmethod

from source.product.domain.entities.product import Product


class IProductSecondaryPort(ABC):
    """Secondary port product"""

    @abstractmethod
    async def save_product(self, product: Product) -> int:
        """Implement save product"""
        raise NotImplementedError("Implement save product")
