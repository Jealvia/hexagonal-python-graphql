from abc import ABC, abstractmethod

from source.product.domain.dtos.product import ProductSchema


class IProductPrimaryPort(ABC):
    """Primary port product"""

    @abstractmethod
    async def save_product(self, product: ProductSchema) -> int:
        """Implement save product"""
        raise NotImplementedError("Implement save product")
