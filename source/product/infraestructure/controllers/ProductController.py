from source.product.application.ports.primary.iproductprimaryport import (
    IProductPrimaryPort,
)
from source.product.application.use_cases.product_usecases import ProductUseCases
from source.product.domain.dtos.product import ProductSchema
from source.product.infraestructure.persistence.implements.ProductSecondaryAdapter import (
    ProductSecondaryAdapter,
)
from fastapi import Depends


class CreateProductController:
    """Product controller"""

    _product_primary_port: IProductPrimaryPort

    def __init__(
        self, productsecondaryadapter: ProductSecondaryAdapter = Depends()
    ) -> None:
        self._product_primary_port = ProductUseCases(productsecondaryadapter)

    async def create_product(self, product: ProductSchema) -> int:
        """Create product"""
        return await self._product_primary_port.save_product(product)
