from asyncpg import create_pool, Connection
from fastapi import Depends
from source.settings import POSTGRESQL_DATABASE


async def create_database_pool():
    """Creation of pool of conections"""
    pool = await create_pool(POSTGRESQL_DATABASE)
    return pool


async def get_database_connection(pool: Connection = Depends(create_database_pool)):
    """Conection instance to database"""
    async with pool.acquire() as connection:
        yield connection
