from source.product.application.ports.secondary.iproductsecondaryport import (
    IProductSecondaryPort,
)
from source.product.domain.entities.product import Product


class ProductSecondaryAdapter(IProductSecondaryPort):
    """Implementation of secondary port"""

    async def save_product(self, product: Product) -> int:
        """Save product implementation"""
        product.id = 10
        return product.id
