import strawberry
from strawberry.types import Info
from source.Graphql import get_product_service

from source.product.domain.dtos.gproduct import GProductIdInput


@strawberry.type(description="Mutate all Entity")
class ProductQuery:
    """Product mutation"""

    @strawberry.field(description="Get one product")
    async def get_product(self, product: GProductIdInput, info: Info) -> int:
        """Register product gql"""
        product_service = get_product_service(info)
        return 1
