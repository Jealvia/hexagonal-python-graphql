import strawberry
from strawberry.types import Info
from source.Graphql import get_product_service

from source.product.domain.dtos.gproduct import GProductSchema
from source.product.domain.dtos.product import ProductSchema


@strawberry.type(description="Mutate all Entity")
class ProductMutation:
    """Product mutation"""

    @strawberry.mutation(description="Register product")
    async def register_product(self, product: GProductSchema, info: Info) -> int:
        """Register product gql"""
        product_service = get_product_service(info)
        result = await product_service.create_product(
            ProductSchema(name=product.name, price=product.price)
        )
        return result
