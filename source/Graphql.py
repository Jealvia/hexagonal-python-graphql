from fastapi import Depends
from strawberry.types import Info
from asyncpg import Connection
from source.product.infraestructure.controllers.ProductController import (
    CreateProductController,
)


async def get_graphql_context(product_service: CreateProductController = Depends()):
    """Definition of gql context"""
    return {
        "productService": product_service,
        # "connection": connection,
    }


def get_product_service(info: Info) -> CreateProductController:
    """Returns instance of productService"""
    return info.context["productService"]


def get_conection(info: Info) -> Connection:
    """Returns a instance of database"""
    return info.context["connection"]
