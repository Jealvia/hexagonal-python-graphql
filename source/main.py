from fastapi import FastAPI
from strawberry import Schema
from strawberry.fastapi import GraphQLRouter
from source import routes
from source.Graphql import get_graphql_context
from strawberry.tools import merge_types
from fastapi.middleware.cors import CORSMiddleware
import debugpy

from source.product.infraestructure.services.gql.mutation.ProductMutation import (
    ProductMutation,
)
from source.product.infraestructure.services.gql.query.ProductQuery import ProductQuery


debugpy.listen(("0.0.0.0", 5678))
# debugpy.wait_for_client


""" MutationCombine = merge_types(
    name="MutationCombine",
    types=ProductMutation,
)
QueryCombine = merge_types(
    name="QueryCombine",
    types=ProductQuery,
) """
app = FastAPI()

origins = [
    "http://localhost:3000",
    "http://localhost:3001",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


schema = Schema(query=ProductQuery, mutation=ProductMutation)

graphql = GraphQLRouter(
    schema,
    graphiql=True,
    context_getter=get_graphql_context,
)

# Integrate GraphQL Application to the Core one
app.include_router(
    graphql,
    prefix="/graphql",
    include_in_schema=False,
)
routes.register(app)
